<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductType extends Model
{
    protected $guarded = ['id'];

    public function product_type_meta()
    {
        return $this->hasMany(ProductTypeMeta::class);
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }
}
