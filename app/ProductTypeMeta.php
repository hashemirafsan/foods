<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductTypeMeta extends Model
{
    protected $guarded = ['id'];

    public function product_type()
    {
        return $this->belongsTo(ProductType::class);
    }
}
