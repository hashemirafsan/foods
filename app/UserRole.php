<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{

    protected $guarded = ['id'];

    protected $hidden = ['created_at', 'updated_at'];

    public function user() {

    	return $this->belongsTo(User::class);

    }

    public function role() {

    	return $this->belongsTo(Role::class);
    }
}
