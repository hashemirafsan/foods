<?php

namespace App\Http\Controllers\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\ProductType;
use App\ProductMeta;
use Carbon\Carbon;

class ProductsController extends Controller
{
    public $request;
    public $product;
    public $productTypes;
    public $productMeta;

    public function __construct
    (
        Request $request,
        Product $product,
        ProductType $productTypes,
        ProductMeta $productMeta
    )
    {
        $this->request = $request;
        $this->product = $product;
        $this->productTypes = $productTypes;
        $this->productMeta = $productMeta;
    }

    public function getAllProduct()
    {
        return response()->json(
            [
                'products' => $this->productTypes->with('products')->get()
            ]
        );
    }

    public function createProduct()
    {
        $productId = $this->product->create(
            [
                'product_type_id'   => $this->request->get('product_type_id'),
                'product_name'      => $this->request->get('product_name'),
                'created_at'        => Carbon::now()->toDateTimeString(),
                'updated_at'        => Carbon::now()->toDateTimeString()
            ]
        );

        $this->productMeta->create(
            [
                'product_id' => $productId->id,
                'meta_key'   => 'product_details',
                'meta_value' => serialize( $this->request->get('dynamic') )
            ]
        );

        return response()->json(
            [
                'status' => 'success',
                'message'  => 'Successfully Created!'
            ]
        );
    }
}
