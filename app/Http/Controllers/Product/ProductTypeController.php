<?php

namespace App\Http\Controllers\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ProductType;
use App\ProductTypeMeta;
use Carbon\Carbon;

class ProductTypeController extends Controller
{
    public $productType;
    public $productTypeMeta;
    public $request;

    public function __construct
    (
        ProductType $productType,
        ProductTypeMeta $productTypeMeta,
        Request $request
    )
    {
        $this->productType = $productType;
        $this->productTypeMeta = $productTypeMeta;  
        $this->request = $request;
    }

    public function getProductTypes()
    {
        $productTypes = $this->productType->with(['product_type_meta'])->orderBy('id', 'DESC')->get();

        foreach($productTypes as $type) {
            if(count($type->product_type_meta) ) {
                foreach($type->product_type_meta as $meta) {
                    $meta->options = unserialize($meta->options);
                }
            }
        }

        return response()->json([
            'product_types' => $productTypes
        ]);
    }


    public function createProductTypes()
    {
        $createProductType = $this->productType->create(
            [
                'product_type_name' => $this->request->get('product_type_name'),
                'status'            => $this->request->get('status'),
                'available'         => $this->request->get('available'),
                'created_at'        => Carbon::now()->toDateTimeString(),
                'updated_at'        => Carbon::now()->toDateTimeString()
            ]
        );

        if ( count( $this->request->get('extra_field') ) ) {
            foreach($this->request->get('extra_field') as $field ){
                $this->productTypeMeta->create(
                    [
                        'product_type_id' => $createProductType->id,
                        'field_type'      => $field['field_type'],
                        'label'           => $field['label'],
                        'options'         => serialize( $field['value'] ),
                        'created_at'      => Carbon::now()->toDateTimeString(),
                        'updated_at'      => Carbon::now()->toDateTimeString()
                    ]
                );
            }
        }

        return response()->json(
            [
                'status' => 'success',
                'message' => 'Successfully Created!'
            ]
        );
    }
    
}
