<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $guarded = ['id'];
    
    protected $hidden = ['created_at', 'updated_at'];

    public function users() {

    	return $this->belongsToMany(User::Class, 'user_role', 'role_id', 'user_id');

    }
}
