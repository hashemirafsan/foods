<?php

/**
 * All Upload url will be here
 */

 Route::group([
     'prefix' => 'upload'
 ], function() {

    Route::post('/image', 'Helper\FileUploadController@uploadImage')->name('upload.image');
    Route::post('/files', 'Helper\FileUploadController@uploadFile')->name('upload.files');

 });