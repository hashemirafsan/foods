<?php

/**
 * All Product Types Here
 * We can reuse this routes in admin
 * or Front of user.
 */

 Route::group([
     'prefix' => 'product-types'
 ], function() {

    Route::get('/all', 'Product\ProductTypeController@getProductTypes')->name('product.types.all');
    Route::post('/create', 'Product\ProductTypeController@createProductTypes')->name('product.type.create');

 });