<?php

/**
 * All Product Types Here
 * We can reuse this routes in admin
 * or Front of user.
 */

 Route::group([
     'prefix' => 'products'
 ], function() {

    Route::get('/all', 'Product\ProductsController@getAllProduct')->name('products.all');
    Route::post('/create', 'Product\ProductsController@createProduct')->name('products.create');

 });