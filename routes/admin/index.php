<?php

/**
 * Admin All Routes here
 */

use Illuminate\Http\Request;

Route::group([
    'prefix' => 'admin'
], function() {

    // Auth
    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::get('/', 'AdminController@index')->name('admin.dashboard');
    Route::get('/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');
    
    // Upload Image And Files
    require __DIR__ . '/../upload/index.php';
    // product type
    require __DIR__ . '/../product/product-types/index.php';
    // products
    require __DIR__ . '/../product/products/index.php';
});