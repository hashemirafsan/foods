require('../app')

import router from './router'
import store from './store'

const app = new Vue({
    router,
    store
}).$mount('#app')