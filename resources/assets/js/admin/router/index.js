import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter);

const productDirRoute = '../components/product/';

import DashboardApp from '../dashboard.vue';

export default new VueRouter({
    routes: [
        {
        	path: '/',
            component: DashboardApp,
            children: [
                
                /**
                 * Product Type Route
                 */

                {
                    path: '/product-types',
                    name: 'product_types',
                    component: require( '../components/product/product_type/view.vue')
                },
                {
                    path: '/product-types/add',
                    name: 'product_types_add',
                    component: require( '../components/product/product_type/add.vue' )   
                },

                /**
                 * Products Route
                 */

                {
                    path: '/products',
                    name: 'products',
                    component: require('../components/product/Products/view.vue')
                },
                {
                    path: '/products/add',
                    name: 'products_add',
                    component: require('../components/product/Products/add.vue')
                }
            ]
        }
    ]
})