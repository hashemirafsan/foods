export default {
    namespaced: true,
    state: {
        product: []
    },
    getters: {
        products: (state) => state.product
    },
    mutations: {
        setAllProducts: (state, data) => {
            state.product = data;
        }
    },
    actions: {
        getAllProducts: ({ commit }) => {
            const productUrl = home.products_all;
            axios.get(productUrl)
                 .then(({ data: { products } }) => {
                    commit('setAllProducts', products);
                 })
                 .catch((err) => {
                     console.log(err)
                 })
        },

        createNewProduct: ({ commit } , data) => {
            const productCreateUrl = home.product_create;
            axios.post(productCreateUrl, data)
                 .then(({ status, message }) => {
                    console.log(status, message)
                 })
                 .catch((err) => {

                 })
        }
    }
}