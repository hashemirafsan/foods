/**
 * All Product Staffs Here
 */

import product_type from './product_type';
import products from './products';

export default {
    namespaced: true,
    modules: {
        product_type,
        products
    }
}