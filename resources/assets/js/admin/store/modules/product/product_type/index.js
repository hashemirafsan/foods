/**
 * Product Type
 */

export default {
    namespaced: true,

    state: {
        fieldType: [
            'input',
            'select',
            'radio',
            'checkbox'
        ],
        productType: []
    },

    getters: {
        fieldTypes: (state) => state.fieldType,
        productTypes: (state) => state.productType
    },

    mutations: {

        setProductTypeToState( state, data ) {
            state.productType = data;
        },

        afterCreateNotify(state, status, message) {
            console.log(status, message)
        }

    },

    actions: {
        getProductTypes({ commit }) {
            const productTypeUrl = home.product_types_all;
            axios.get(productTypeUrl)
                 .then(( { data: { product_types } } ) => {
                    commit('setProductTypeToState', product_types);
                 })
                 .catch((err) => {
                     console.log(err);
                 })
        },

        createProductTypes({ commit }, data) {
            const productTypeCreateUrl = home.product_type_create;
            axios.post(productTypeCreateUrl, data)
                 .then(({ data: { status, message } }) => {
                    commit('afterCreateNotify', status, message);
                 })
                 .catch((err) => {
                     console.log(err)
                 })
        }
    }
}