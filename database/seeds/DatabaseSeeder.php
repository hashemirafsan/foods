<?php

use Illuminate\Database\Seeder;
use App\Admin;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $admin1 = Admin::create([
            'name' => 'Hashemi Rafsan',
            'email' => 'rafsan@gmail.com',
            'password' => bcrypt('secret')
        ]);
        // $this->call(UsersTableSeeder::class);
    }
}
